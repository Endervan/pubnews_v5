export interface Dados {
    nome: string;
}

//
export interface Parceiro {
    nome: string;
    email: string;
    cpf: string;
    celular: string;
    foto: string;
    tipo: string;
}


export interface Anuncios {
    idanuncio?: number;
    titulo?: string;
    arquivo?: string;
    qtd_exibicoes_contratadas?: number;
    qtd_exibicoes_efetuadas?: number;
    pagamento_aprovado?: string;
    data_inicio?: any;
    data_fim?: any;
    arquivo_size?: string;
    url_arquivo: string;

}
