import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {UtilService} from './services/util/util.service';

const routes: Routes = [
    {
        path: '*',
        redirectTo: 'social',
        pathMatch: 'full'
    },
    {
        path: 'social',
        loadChildren: () => import('./login/social/social.module').then(m => m.SocialPageModule)
    },
    {
        path: 'cadastro',
        loadChildren: () => import('./login/cadastro/cadastro.module').then(m => m.CadastroPageModule)
    },
    {
        path: 'esqueceu-senha',
        loadChildren: () => import('./login/esqueceu-senha/esqueceu-senha.module').then(m => m.EsqueceuSenhaPageModule)
    },
    {
        path: 'conexao-status', loadChildren: './conexao-status/conexao-status.module#ConexaoStatusPageModule',
        canActivate: [UtilService] // rota protegida
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
