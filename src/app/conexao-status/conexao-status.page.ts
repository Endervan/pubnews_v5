import {Component, OnInit} from '@angular/core';
import {Downloader, DownloadRequest, NotificationVisibility} from '@ionic-native/downloader/ngx';
import {UtilService} from '../services/util/util.service';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../services/auth.service';
import {Anuncios} from '../interface/dados';
import {DatePipe} from '@angular/common';
import {HttpService} from '../services/http.service';
import {take} from 'rxjs/operators';


@Component({
    selector: 'app-conexao-status',
    templateUrl: './conexao-status.page.html',
    styleUrls: ['./conexao-status.page.scss'],
    providers: [DatePipe]
})

export class ConexaoStatusPage implements OnInit {

    ArquivosAnuncios: Anuncios[]; // dados Anuncios  VIA GET vem json

    getStorage: any;

    constructor(private downloader: Downloader, public util: UtilService,
                private activedRoute: ActivatedRoute, private authService: AuthService,
                public datepipe: DatePipe, private httpService: HttpService) {


    }


    async ngOnInit() {

        await this.util.tipoConexao();
        await this.util.getPositicao();


        // dado token storage
        this.util.tokenStorage = await this.util.getParceiroStorage('Parceiro');
        this.util.tokenStorage = this.util.tokenStorage.token;
        // console.log('aqui', this.util.tokenStorage);

        // populando dados Anuncios parceiro
        await this.getAnunciosParceiro();

        // populando dados parceiro
        await this.getDadosParceiro();
    }


    // preste a entra pagina existir token storage faz redirect conexao-status
    async ionViewWillEnter(): Promise<void> {
        // abilitando o menu
        await this.util.menu(true);
        const loading = await this.util.loading();
        try {
            // console.log('storage -> ', this.util.tokenStorage, '===', 'json ->', this.util.tokenJson);
            if (this.util.tokenStorage !== this.util.tokenJson) {
                await this.util.rootPagina(`social`);
            }
        } catch (e) {
            console.log(e);
        } finally {
            await loading.dismiss();
        }
    }


    // funcao downloader native
    async Donwloader(url: string, nome: any, arquivo: any) {
        // console.log('dados arquivos download == ', url, nome, arquivo);

        const request: DownloadRequest = {
            uri: url,
            title: nome,
            description: '',
            mimeType: '',
            visibleInDownloadsUi: true,
            notificationVisibility: NotificationVisibility.VisibleNotifyCompleted,
            destinationInExternalFilesDir: {
                dirType: 'Downloads',
                subPath: arquivo
            }
        };
        this.downloader.download(request)
            .then((location: string) => console.log('Baixando download em: ' + location))
            .catch((error: any) => console.log(error));

    }

    // dados parceiro
    getDadosParceiro() {
        this.httpService.getUsuario(this.util.tokenStorage)
            .pipe(take(1))
            .subscribe((data: any) => {
                // console.log('dados parceiros', data);
                this.util.nomeParceiro = data.nome;
                this.util.emailParceiro = data.email;
                this.util.cpfParceiro = data.cpf;
                this.util.celularParceiro = data.celular;
                this.util.fotoParceiro = data.foto;
                this.util.tipoParceiro = data.tipo_parceiro;
                this.util.tokenJson = data.token;
            }, err => {
                console.log(err);
            });
    }

    async getAnunciosParceiro() {
        this.httpService.getArquivosAnuncios(this.util.longitude, this.util.latitude, this.util.tokenStorage)
            .pipe(take(1))
            .subscribe(async (data) => {
                // arquivos mostrar via get(json)  ////
                this.ArquivosAnuncios = data;
                // console.log('dados anuncios json', this.ArquivosAnuncios);

                for (let i = 0; i < (Object.keys(data).length); i++) {
                    const x = i;
                    // //////// dados pra donwload //////////
                    this.util.urlAnuncio = data[x].url_arquivo;
                    this.util.titleAnuncio = data[x].titulo;
                    this.util.arquivoAnuncio = data[x].arquivo;
                    this.util.idAnuncio = data[x].idanuncio;

                    ///// dados setado storage  anuncios ///////////
                    await this.util.setAnunciosStorage(this.util.titleAnuncio,
                        this.util.idAnuncio, this.util.titleAnuncio, this.util.arquivoAnuncio, this.util.urlAnuncio);


                    // mostrar arquivos  via storage ja setado=////
                    // this.getStorage = await this.util.getParceiroStorage(this.util.titleAnuncio);
                    // console.log('get array storage', this.getStorage);

                    // arquivos baixados celular dinamicamente ////
                    await this.Donwloader(this.util.urlAnuncio, this.util.titleAnuncio, this.util.arquivoAnuncio);
                }
            }, err => {
                console.log(err);
            });
    }
}


