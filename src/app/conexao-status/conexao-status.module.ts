import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {ConexaoStatusPageRoutingModule} from './conexao-status-routing.module';

import {ConexaoStatusPage} from './conexao-status.page';
import {LogoutComponent} from '../components/logout/logout.component';
import {MenuButtonComponent} from '../components/menu-button/menu-button.component';

@NgModule({
    declarations: [ConexaoStatusPage, LogoutComponent, MenuButtonComponent],
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ConexaoStatusPageRoutingModule,
    ],
    exports: [LogoutComponent, MenuButtonComponent]
})
export class ConexaoStatusPageModule {
}
