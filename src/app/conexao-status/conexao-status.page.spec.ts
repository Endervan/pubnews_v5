import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ConexaoStatusPage } from './conexao-status.page';

describe('ConexaoStatusPage', () => {
  let component: ConexaoStatusPage;
  let fixture: ComponentFixture<ConexaoStatusPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConexaoStatusPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ConexaoStatusPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
