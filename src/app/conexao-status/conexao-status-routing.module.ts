import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConexaoStatusPage } from './conexao-status.page';

const routes: Routes = [
  {
    path: '',
    component: ConexaoStatusPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConexaoStatusPageRoutingModule {}
