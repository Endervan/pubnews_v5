import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {Facebook} from '@ionic-native/facebook/ngx';
import {GooglePlus} from '@ionic-native/google-plus/ngx';
import {HttpClientModule} from '@angular/common/http';
import {IonicStorageModule} from '@ionic/storage';
import {Network} from '@ionic-native/network/ngx';
import {Dialogs} from '@ionic-native/dialogs/ngx';
import {Downloader} from '@ionic-native/downloader/ngx';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {HTTP} from '@ionic-native/http/ngx';
import {UtilService} from './services/util/util.service';
import {ConexaoStatusPageModule} from './conexao-status/conexao-status.module';

@NgModule({
    declarations: [AppComponent],
    entryComponents: [],
    imports: [
        BrowserModule,
        IonicModule.forRoot(),
        AppRoutingModule,
        HttpClientModule,
        IonicStorageModule.forRoot(),
        ConexaoStatusPageModule,
    ],
    providers: [
        StatusBar,
        SplashScreen,
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
        Facebook,
        GooglePlus,
        HttpClientModule,
        Network,
        Dialogs,
        Downloader,
        Geolocation,
        HTTP,
        UtilService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
