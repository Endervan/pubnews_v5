import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NavController} from '@ionic/angular';
import {ValidatorService} from '../../services/validator.service';
import {AuthService} from '../../services/auth.service';
import {HttpService} from '../../services/http.service';
import {UtilService} from '../../services/util/util.service';

@Component({
    selector: 'app-cadastro',
    templateUrl: './cadastro.page.html',
    styleUrls: ['./cadastro.page.scss'],
})
export class CadastroPage implements OnInit {
    form: FormGroup;

    // private isPasswordSame: boolean;


    constructor(public formBuilder: FormBuilder, public util: UtilService,
                private val: ValidatorService, private authService: AuthService,
                private navCtrl: NavController) {

        //  campos do builder para controle
        this.form = formBuilder.group({
            nome: ['', Validators.required, this.val.nameValid],
            celular: ['', Validators.compose([Validators.minLength(14), Validators.required])],
            email: ['', Validators.required, this.val.emailValid],
            tipo_parceiro: ['', Validators.compose([Validators.minLength(1), Validators.required])],
            senha: ['', Validators.compose([Validators.minLength(6), Validators.required])],
            // confirmPassword: ['', Validators.compose([Validators.required])],
        },  /*{validator: this.checkPassword('password', 'confirmPassword')}*/);
    }

    // promise check se senha digitadas sao identicas
    // public  checkPassword(controlName: string, matchingControlName: string) {
    //     return (formGroup: FormGroup) => {
    //         const control = formGroup.controls[controlName];
    //         const matchingControl = formGroup.controls[matchingControlName];
    //         if (matchingControl.errors && !matchingControl.errors.mustMatch) {
    //             // return if another validator has already found an error on the matchingControl
    //             return;
    //         }
    //         // set error on matchingControl if validation fails
    //         if (control.value !== matchingControl.value) {
    //             matchingControl.setErrors({mustMatch: true});
    //             this.isPasswordSame = (matchingControl.status == 'VALID') ? true : false;
    //         } else {
    //             matchingControl.setErrors(null);
    //             this.isPasswordSame = (matchingControl.status == 'VALID') ? true : false;
    //         }
    //     };
    // }

    async ngOnInit() {
    }


    // Cadastrar parceiro
    async Cadastrar() {

        this.authService.signup(this.form.value).subscribe((data: any) => {
            console.log(data);
            if (data.status) {
                // this.util.toast({message: data.mensagem, buttons: ['ok']}); //  msg de success aplicativo
                this.util.setParceiroStorage(data.token);
                this.util.irPagina(`conexao-status`);
            } else {
                this.util.alert({message: data.mensagem, buttons: ['OK']}); //  msg erro aplicativo
                console.log(data.mensagem);
            }
        }, err => {
            this.util.toast({message: err.message, buttons: ['OK']}); //  msg de erro api
            console.log(err);
        });
    }

}

