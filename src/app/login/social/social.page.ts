import {Component, OnInit} from '@angular/core';
import {Facebook} from '@ionic-native/facebook/ngx';
import {GooglePlus} from '@ionic-native/google-plus/ngx';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ValidatorService} from '../../services/validator.service';
import {AuthService} from '../../services/auth.service';
import {StorageService} from '../../services/storage.service';
import {UtilService} from '../../services/util/util.service';
import {HttpService} from '../../services/http.service';

@Component({
    selector: 'app-social',
    templateUrl: './social.page.html',
    styleUrls: ['./social.page.scss'],
    providers: [GooglePlus, ValidatorService]
})
export class SocialPage implements OnInit {
    //  instanciando  formgroup
    form: FormGroup;


    // face develops dados api
    isLoggedIn = false;
    users = {id: '', name: '', email: '', picture: {data: {url: ''}}};

    constructor(private fb: Facebook, private googlePlus: GooglePlus, private  router: Router,
                public formBuilder: FormBuilder, private storageService: StorageService, private val: ValidatorService,
                private authService: AuthService, public util: UtilService, private httpService: HttpService) {


        // verificando status conexão com face
        fb.getLoginStatus()
            .then(res => {
                console.log(res.status);
                if (res.status === 'connect') {
                    this.isLoggedIn = true;
                } else {
                    this.isLoggedIn = false;
                }
            })
            .catch(e => console.log(e));

        //  campos do builder para controle
        this.form = formBuilder.group({
            email: ['', Validators.required, this.val.emailValid],
            senha: ['', [Validators.required, Validators.minLength(6)]],
        });
    }

    async ngOnInit() {
        //  desabilitando o menu
        await this.util.menu(false);
        this.util.tokenStorage = await this.util.getParceiroStorage('Parceiro');
        this.util.tokenStorage = this.util.tokenStorage.token;

        // verifica token json
        if (this.util.tokenStorage) {
            this.util.getTokenParceiroJson(this.util.tokenStorage);
        }
    }


    // Obs: token storage == tokenJson (usuario logado) faz redirect
    // preste a entra pagina existir token storage faz redirect conexao-status/token
    async ionViewDidEnter(): Promise<void> {
        const loading = await this.util.loading();
        try {
            // console.log('storage -> ', this.util.tokenStorage, '===', 'json ->', this.util.tokenJson);
            if (this.util.tokenJson) {
                await this.util.irPagina(`conexao-status`);
            }
        } catch (e) {
            console.log(e);
        } finally {
            await loading.dismiss();
        }
    }


    // validateInputs() {
    //     const email = this.form.value.email.trim();
    //     const password = this.form.value.senha.trim();
    //
    //     return (
    //         this.form.value.email &&
    //         this.form.value.senha &&
    //         email.length > 0 &&
    //         password.length > 0
    //     );
    //
    // }


    // login normal
    async loginAction() {
        this.authService.login(this.form.value).subscribe((data: any) => {
            if (data.status === false) {
                this.util.toast({message: data.mensagem, buttons: ['OK'], position: 'top'}); //  msg de erro retorno api
            } else {
                // setando dados Parceiro offline storage
                this.util.setParceiroStorage(data.token, data.nome, data.cpf, data.celular, data.email, data.foto, data.tipo_parceiro);

                this.util.irPagina(`conexao-status`);
                this.util.menu(true);
            }
        }, err => {
            this.util.toast({message: 'OffLine', buttons: ['OK'], position: 'top'}); //  msg de erro
            console.log(err);
        });
    }


    // login facebook
    fbLogin() {
        this.fb.login(['public_profile', 'user_friends', 'email'])
            .then(res => {
                if (res.status === 'connected') {
                    this.isLoggedIn = true;
                    this.getUserDetail(res.authResponse.userID);
                } else {
                    this.isLoggedIn = false;
                }
            })
            .catch(e => console.log('Error logging into Facebook', e));
    }

    // buscando dados assicronos FACEBOOK
    getUserDetail(userid: any) {
        this.fb.api('/' + userid + '/?fields=id,email,name,picture', ['public_profile'])
            .then(res => {
                console.log(res);
                this.users = res;
            })
            .catch(e => {
                console.log(e);
            });
    }

    // funcao sair e resetar cache login FACEBOOK
    logoutFace() {
        this.fb.logout()
            .then(res => this.isLoggedIn = false)
            .catch(e => console.log('Error logout from Facebook', e));
    }


// login google plus
    googleLogin() {
        this.googlePlus.login({}).then(res => {
            console.log('dados api google' + res);
        })
            .catch(err => {
                console.error('erros ao busca dados ' + err);
            });
    }
}
