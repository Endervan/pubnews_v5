import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UtilService} from '../../services/util/util.service';
import {ValidatorService} from '../../services/validator.service';
import {AuthService} from '../../services/auth.service';

@Component({
    selector: 'app-esqueceu-senha',
    templateUrl: './esqueceu-senha.page.html',
    styleUrls: ['./esqueceu-senha.page.scss'],
})
export class EsqueceuSenhaPage implements OnInit {
    form: FormGroup;

    constructor(public formBuilder: FormBuilder, public util: UtilService,
                private val: ValidatorService, private authService: AuthService) {

        //  campos do builder para controle
        this.form = formBuilder.group({
            email: ['', Validators.required, this.val.emailValid]
        });
    }


    ngOnInit() {
    }


    // Recupera Senha
    async Recuperar() {
        const loading = await this.util.loading();
        this.authService.recuperaSenha(this.form.value).subscribe((data: any) => {

            try {
                if (data.status === true) {
                    this.util.toast({message: data.mensagem, buttons: ['OK']});  //  msg de sucesso
                    this.util.irPagina('/social');
                } else {
                    this.util.alert({message: data.mensagem, buttons: ['OK']});  //  erro dados invalidos
                }
            } catch (err) {
                this.util.toast({message: err.mensagem, buttons: ['OK']}); //  msg de erro
                console.log(err);
            } finally {
                loading.dismiss();
            }
        });
    }


}
