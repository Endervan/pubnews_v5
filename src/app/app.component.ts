import {Component, OnInit} from '@angular/core';

import {Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {UtilService} from './services/util/util.service';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
    pages: { url: string, direction: string, icon: string, text: string }[];
    public selectedIndex = 0;

    // dados parceiros
    nomeParceiro;
    emailParceiro;
    celularParceiro;

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        public util: UtilService,
    ) {
        this.initializeApp();
        this.pages = [
            {url: 'teste', direction: 'back', icon: 'checkmark', text: 'Teste'},
            {url: 'social', direction: 'forward', icon: 'add', text: 'Login'},
        ];
    }


    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });
    }

    async ngOnInit() {
        this.nomeParceiro = await this.util.getItem('nome');
        this.emailParceiro = await this.util.getItem('email');
        this.celularParceiro = await this.util.getItem('celular');
    }

}


