import {Injectable} from '@angular/core';
import {FormControl} from '@angular/forms';

@Injectable({
    providedIn: 'root'
})
export class ValidatorService {

    constructor() {
    }

    // validacao email
    emailValid(control: FormControl) {
        return new Promise(resolve => {
            const emailPattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

            if (!emailPattern.test(control.value)) {
                resolve({InvalidEmail: true});
            }
            resolve(null);
        });
    }

    // validacao nomes
    nameValid(control: FormControl) {
        return new Promise(resolve => {
            // somente letras
            const pattern = /[0-9]/;
            if (pattern.test(control.value)) {
                resolve({InvalidName: true});
            }
            resolve(null);
        });
    }

    // validacao cpf
    // cpfValid(control: FormControl) {
    //     return new Promise(resolve => {
    //         // somente numeros
    //         const pattern = /(\d{3})(\d{3})(\d{3})(\d{2})/;
    //         if (pattern.test(control.value)) {
    //             resolve({InvalidCpf: true});
    //         }
    //         resolve(null);
    //     });
    // }
}

