import { TestBed } from '@angular/core/testing';

import { UtilService } from './util.service';
import {ValidatorService} from '../validator.service';

describe('UtilService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UtilService = TestBed.get(UtilService);
    expect(service).toBeTruthy();
  });
});
