import {Injectable, OnInit} from '@angular/core';
import {AlertController, LoadingController, MenuController, NavController, ToastController} from '@ionic/angular';
import {AlertOptions, LoadingOptions, ToastOptions} from '@ionic/core';
import {NetworkStatus, PluginListenerHandle, Plugins} from '@capacitor/core';
import {ActivatedRouteSnapshot, CanActivate} from '@angular/router';
import {take} from 'rxjs/operators';
import {HttpService} from '../http.service';


const {Geolocation, Network, Storage} = Plugins;

export interface Parceiro {
    dadosNome: string;
    dadosEmail: string;
    dadosCpf: number;
    dadosCelular: number;
    dadosFoto: string;
    tipoParceiro: string;
}

@Injectable({
    providedIn: 'root'
})
export class UtilService implements OnInit, CanActivate {
    // network Capacitor
    networkStatus: NetworkStatus;
    networkListener: PluginListenerHandle;
    public request;

    //////// DADOS ANUNCIOS VIA STORAGE CAPACITOR OFFLINE///////////
    titleAnuncio: any;
    arquivoAnuncio: any;
    idAnuncio: number;
    urlAnuncio: any;
//////// DADOS ANUNCIOS VIA STORAGE CAPACITOR OFFLINE///////////

    // dados parceiros
    nomeParceiro?: string;
    cpfParceiro?: number;
    celularParceiro?: number;
    emailParceiro?: string;
    fotoParceiro?: null;
    tipoParceiro?: string;
    //////////// ==== //////////////
    tokenStorage: any = null; // vem storage
    tokenJson: any; // vem api

    // coords
    public longitude: number;
    public latitude: number;


    constructor(public  navCtrl: NavController, public loadingController: LoadingController, private  toastController: ToastController,
                public alertCtrl: AlertController, private menuCtrl: MenuController, private httpService: HttpService) {
    }

    async ngOnInit() {

    }

    // guarda route usuario autenticados token storage armazenado
    async canActivate(route: ActivatedRouteSnapshot, token) {
        // if (this.tokenStorage) {
        //     return true;
        // }
        // await this.irPagina([`social`]);
        // return false;

        return true;
    }

    // dado token json
    getTokenParceiroJson(token) {
        this.httpService.getUsuario(token).pipe(take(1)).subscribe((data: any) => {
            this.tokenJson = data.token;
        }, err => {
            console.log(err);
        });
    }

    ///// Storage /////////////////////////

    // idanuncio vai ser idToken ver marcio //////////////////////
    // setando dados array
    async setAnunciosStorage(keyAnuncio, idanuncio, Titulo, arquivo, urlArquivo) {
        await Storage.set({
            key: keyAnuncio,
            value: JSON.stringify([{
                Id: idanuncio,
                Arquivo: arquivo,
                titulo: Titulo,
                Url: urlArquivo
            }])
        });
    }

    async setParceiroStorage(tokenParceiro, nomeParceiro?, cpfParceiro?, celularParceiro?, emailParceiro?, fotoParceiro?, tipoParceiro?) {
        await Storage.set({
            key: 'Parceiro',
            value: JSON.stringify({
                token: tokenParceiro,
                nome: nomeParceiro,
                cpf: cpfParceiro,
                celular: celularParceiro,
                email: emailParceiro,
                foto: fotoParceiro,
                tipo_Parceiro: tipoParceiro,

            })
        });
    }

    // buscando dados array
    async getParceiroStorage(chave) {
        const dado = await Storage.get({key: chave});
        const dados = JSON.parse(dado.value);
        return dados;
    }

    // set dado individual
    async setItem(dadoKey, dadoValue): Promise<any> {
        await Storage.set({key: dadoKey, value: dadoValue});
    }

    // pegando dado
    async getItem(dadoKey): Promise<string> {
        const dados = await Storage.get({key: dadoKey});
        const dadosStorage = dados.value;
        return dadosStorage;
    }

    // removendo dado
    async removeItem(dado) {
        await Storage.remove({key: dado});
    }

    // pegando somente dados keys
    async getkeys() {
        const keys = await Storage.keys();
        console.log('keys', keys);
    }

    // limpando  Tudo Storage
    async clearStorage() {
        await Storage.clear();
    }

    ///// Storage /////////////////////////

    // sair aplicativo
    async logout() {
        await Storage.clear();
    }

    // loading processos
    async loading(options?: LoadingOptions): Promise<HTMLIonLoadingElement> {
        const loading = await this.loadingController.create({
            message: 'Carregando...',
            ...options, // passa todos paramentos complementares
        });
        await loading.present();
        return loading;
    }

    // Toast processos
    async toast(options?: ToastOptions): Promise<HTMLIonToastElement> {
        const toast = await this.toastController.create({
            position: 'bottom',
            duration: 3000,
            keyboardClose: true,
            ...options, // passa todos paramentos complementares facultativos
        });
        await toast.present();
        return toast;
    }


    // mensagem de alerta atributos facultativo
    async alert(options?: AlertOptions): Promise<HTMLIonAlertElement> {
        const alert = await this.alertCtrl.create(options);
        await alert.present();
        return alert;
    }


    // monta pilha e vai proxima pagina
    async irPagina(pagina) {
        await this.navCtrl.navigateForward(pagina);
        return;
    }

    //  vai  pagina root
    async rootPagina(pagina) {
        await this.navCtrl.navigateRoot(pagina);
        return;
    }

    // desmonta pilha e volta  pagina
    async voltaPagina(pagina) {
        await this.navCtrl.navigateBack(pagina);
    }

    // habilitar e desabilitar menu
    async menu(menu: boolean): Promise<void> {
        await this.menuCtrl.enable(menu);
        return;
    }

    // geoloaction Com Capacitor
    async getPositicao() {
        const coordinates = await Geolocation.getCurrentPosition();
        //  console.log('Current', coordinates);

        this.latitude = coordinates.coords.latitude;
        this.longitude = coordinates.coords.longitude;
    }

    async tipoConexao() {
        // abilitando providers capacitor status rede conectada
        this.networkListener = Network.addListener('networkStatusChange', status => {
                console.log('Coneção Status ==', status);

                setTimeout(() => {
                    if (this.networkStatus.connectionType === 'wifi') {
                        console.log('we got a wifi connection, woohoo!');
                        this.networkStatus = status;
                    }
                }, 3000);
                // this.networkStatus = status;
            }
        );
        //  buscando status conexao
        this.networkStatus = await Network.getStatus();
    }
}
