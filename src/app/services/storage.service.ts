import {Injectable} from '@angular/core';
import {Plugins} from '@capacitor/core';

const {Storage} = Plugins;

@Injectable({
    providedIn: 'root'
})
export class StorageService {

    constructor() {
    }

    async store(StorageKey: string, value: any) {
        const encryptedValue = btoa(JSON.stringify(value));
        // const encryptedValue = JSON.stringify(value);
        console.log(encryptedValue)
        await Storage.set({
            key: StorageKey,
            value: encryptedValue
        });
    }

    async get(storageKey: string) {
        const res = await Storage.get({key: storageKey});
        if (res.value) {
            return JSON.parse(unescape(atob(res.value)));
        } else {
            return false;
        }
    }

    async removeItem(storageKey: string) {
        await Storage.remove({key: storageKey});
    }

    async clear() {
        await Storage.clear();
    }
}
