import {Component} from '@angular/core';
import {NavController} from '@ionic/angular';
import {UtilService} from '../../services/util/util.service';
import {HttpService} from '../../services/http.service';

@Component({
    selector: 'app-logout',
    template: `
        <ion-buttons>
            <ion-button (click)="logout()">
                <ion-icon name="exit" slot="icon-only"></ion-icon>
            </ion-button>
        </ion-buttons>
    `,
})
export class LogoutComponent {

    constructor(private navCtrl: NavController, public util: UtilService, public httpService: HttpService) {
    }

    async logout(): Promise<void> {
        await this.util.alert({
            message: 'Deseja Sair Aplicação ? ',
            buttons: [
                {
                    text: 'Sim',
                    handler: async () => {
                        await this.util.logout();
                        await this.navCtrl.navigateRoot('/social');
                    }
                },
                'Nao'
            ],
        });
    }

}
